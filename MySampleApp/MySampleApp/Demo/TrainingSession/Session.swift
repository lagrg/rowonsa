//
//  ViewController.swift
//  Splitometer
//
//  Created by Johann Pardanaud on 29/09/2015.
//  Copyright © 2015 Johann Pardanaud. All rights reserved.
//


import Foundation
import UIKit
import CoreBluetooth
import CoreMotion
import AWSDynamoDB
import AWSMobileHubHelper


class Session: UIViewController,CBCentralManagerDelegate, CBPeripheralDelegate, SplitManagerDelegate {
        var centralManager:CBCentralManager!
    var connectingPeripheral:CBPeripheral!
     let  chronometer = Chronometer()
    public var a = [0]
    public var sessionid = 1
    
    var countsks = Int()
    var initsec = Int()
    var endsec = Int()
    var diffsec = Int()
    var strokerate = Int()
    var motionManager = CMMotionManager()
    
    
    var table: Table?
    var timer = Timer()
    
    var splitmin = Int()
    var splitsec = Int()
    var heartr = Int()
    
    @IBOutlet weak var countsk: UILabel!
    @IBOutlet weak var stroker: UILabel!
    
    @IBOutlet weak var chrono: UILabel!
    
    @IBOutlet weak var min: UILabel!
    
    @IBOutlet weak var sec: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    
    
    @IBAction func startchrono(_ sender: UIButton) {
        Timer.scheduledTimer(timeInterval: 0.1, target: self,
                            selector: #selector(Session.updateChrono(_:)), userInfo: nil, repeats: true)
        chronometer.start()
        table?.insertDataHandler?(IuserId: " ", Iaccx: 1, Iaccy: 1, Iaccz: 1, Icategory: " ", Ihr: heartr, Ilatitude: 132, Ilongitude: 123, Imagx: 2, Imagy: 2, Imagz: 2, Iname: " ", Ispeedmin: splitmin, Ispeedsec: splitsec, Itime: 1)
        table?.removeSampleDataWithCompletionHandler?({(errors: [NSError]?) -> Void in
                        var message: String = "All sample items were successfully removed from your table."
            if errors != nil {
                message = "Failed to remove sample items from your table."
            }
            let alartController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let dismissAction: UIAlertAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
            alartController.addAction(dismissAction)
            self.present(alartController, animated: true, completion: nil)
        })
        
        
        /*
         unc insertDataHandler(IuserId: String, Iaccx: Int, Iaccy: Int, Iaccz: Int, Icategory: String, Ihr: Int, Ilatitude: Int, Ilongitude: Int, Imagx: Int, Imagy: Int, Imagz: Int, Iname: String, Ispeedmin: Int, Ispeedsec: Int, Itime: Int)
         
        ({(errors: [NSError]?) -> Void in
            self.activityIndicator.stopAnimating()
            var message: String = "Session Started"
            if errors != nil {
                message = "Ups..Rushing the slide."
                
            }
            let alartController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
            alartController.addAction(dismissAction)
            self.present(alartController, animated: true, completion: nil)
        }) */

        
        
    }
    
    @IBAction func stopchrono(_ sender: UIButton) {
        chronometer.stop()
    }
    
    
    
  
    
    let HRSERVICE = "180D"
    let HRDEVICE = "180A"
   

    
    let splitManager = SplitManager()
    
    override func viewDidLoad() {
        

        
        
        //upload to DDB every given interval
        scheduledTimerWithTimeInterval()
        
        
        motionManager.accelerometerUpdateInterval = 0.2
        
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data
            {
                if myData.acceleration.z > 1
                {
                    print ("FORWARD")
                    let date = Date()
                    let calendar = Calendar.current
                    
                    let seconds = calendar.component(.second, from: date)
                    self.initsec = seconds
                    print(self.initsec)
                   
                    
                }
                if myData.acceleration.z < -1
                {
                    print ("BACkWAERD")
                    let date = Date()
                    let calendar = Calendar.current
                    
                    let seconds = calendar.component(.second, from: date)
                    self.endsec = seconds
                    self.diffsec = self.initsec - self.endsec
                    print(self.endsec)
                    self.strokerate = abs(60/(self.diffsec))
                    self.stroker?.text = String(self.strokerate)
                    print(self.diffsec)
                     self.countsks += 1
                    self.countsk?.text = String(self.countsks)
                    
                }
                
            }
        }

        
        
        let heartRateServiceUUID = CBUUID(string: HRSERVICE)
        let deviceInfoServiceUUID = CBUUID(string: HRDEVICE)
        
        let services = [heartRateServiceUUID, deviceInfoServiceUUID];
        
        let centralManager = CBCentralManager(delegate: self, queue: nil)
        
        centralManager.scanForPeripherals(withServices: services, options: nil)
        
        self.centralManager = centralManager;
        
        splitManager.delegate = self
        super.viewDidLoad()
        super.viewDidLoad()
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("--- centralManagerDidUpdateState")
        switch central.state{
        case .poweredOn:
            print("poweredOn")
            
            let serviceUUIDs:[AnyObject] = [CBUUID(string: "180D")]
            let lastPeripherals = centralManager.retrieveConnectedPeripherals(withServices: serviceUUIDs as! [CBUUID])
            
            if lastPeripherals.count > 0{
                let device = lastPeripherals.last! as CBPeripheral;
                connectingPeripheral = device;
                centralManager.connect(connectingPeripheral, options: nil)
            }
            else {
                centralManager.scanForPeripherals(withServices: serviceUUIDs as? [CBUUID], options: nil)
                
            }
        case .poweredOff:
            print("off")
        case .resetting:
            print("resetting")
        case .unauthorized:
            print("No autorizado")
        case .unknown:
            print("cs unknown")
        case .unsupported:
            print("cs unsupported")
        }
    }
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function **Countdown** with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Session.updateCounting), userInfo: nil, repeats: true)
    }
    func updateCounting(){
        table?.insertDataHandler?(IuserId: " ", Iaccx: 1, Iaccy: 1, Iaccz: 1, Icategory: " ", Ihr: heartr, Ilatitude: 132, Ilongitude: 123, Imagx: 2, Imagy: 2, Imagz: 2, Iname: " ", Ispeedmin: splitmin, Ispeedsec: splitsec, Itime: 1)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Nuevo!")
        
        if let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
            print("HRM: \(localName)")
            self.centralManager.stopScan()
            connectingPeripheral = peripheral
            connectingPeripheral.delegate = self
            centralManager.connect(connectingPeripheral, options: nil)
        }else{
            print("!!!data?!!!")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Conectado")
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        print("HRM estado: \(peripheral.state)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if (error) != nil{
            print("error: \(error?.localizedDescription)")
        }
        else {
            print("error en didDiscoverServices")
            for service in peripheral.services as [CBService]!{
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if (error) != nil{
            print("error en didDiscoverCharacteristicsFor: \(error?.localizedDescription)")
        }
        else {
            
            if service.uuid == CBUUID(string: "180D"){
                for characteristic in service.characteristics! as [CBCharacteristic]{
                    switch characteristic.uuid.uuidString{
                        
                    case "2A37":
                        // Set notification on heart rate measurement
                        print("Found a Heart Rate Measurement Characteristic")
                        peripheral.setNotifyValue(true, for: characteristic)
                        
                    case "2A38":
                        // Read body sensor location
                        print("Found a Body Sensor Location Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A29":
                        // Read body sensor location
                        print("Found a HRM manufacturer name Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A39":
                        // Write heart rate control point
                        print("Found a Heart Rate Control Point Characteristic")
                        
                        var rawArray:[UInt8] = [0x01];
                        let data = NSData(bytes: &rawArray, length: rawArray.count)
                        peripheral.writeValue(data as Data, for: characteristic, type: CBCharacteristicWriteType.withoutResponse)
                        
                    default:
                        print()
                    }
                    
                }
                
            }
            
        }
        
    }

    func update(heartRateData:Data){
        
        print("--- UPDATING ..")
        var buffer = [UInt8](repeating: 0x00, count: heartRateData.count)
        heartRateData.copyBytes(to: &buffer, count: buffer.count)
        
        // UIImage *image = [UIImage imageNamed:@"The-heart.png"];
        // UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        //  [UIView animateWithDuration:0.2f delay:3.0f options:0
        //  animations:^{imageView.alpha = 0.0;}
        //  completion:^{[imageView removeFromSuperview];}];
        
        
        
        var bpm:UInt16?
        if (buffer.count >= 2){
            if (buffer[0] & 0x01 == 0){
                bpm = UInt16(buffer[1]);
            }else {
                bpm = UInt16(buffer[1]) << 8
                bpm =  bpm! | UInt16(buffer[2])
            }
        }
        
        if let actualBpm = bpm{
            print(actualBpm)
            a+=[Int(actualBpm)]
             table?.insertDataHandler?(IuserId: " ", Iaccx: 1, Iaccy: 1, Iaccz: 1, Icategory: " ", Ihr: heartr, Ilatitude: 132, Ilongitude: 123, Imagx: 2, Imagy: 2, Imagz: 2, Iname: " ", Ispeedmin: splitmin, Ispeedsec: splitsec, Itime: 1)
            heartr = Int(actualBpm)
            label1.text = ("\(actualBpm)")
            
            
            print(a)
        }else {
            label1.text = ("\(bpm!)")
            print(bpm!)
        }
        
    }
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateValueForCharacteristic")
        
        if (error) != nil{
            
        }else {
            switch characteristic.uuid.uuidString{
            case "2A37":
                update(heartRateData:characteristic.value!)
                
            default:
                print("diferente a  2A37 uuid charac")
            }
        }
    }

    private func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    private func shouldAutorotate() -> Bool {
        return true
    }
    
    func SplitDidChangetr<T>(_ Splitm: T, _ Splits: T) {
        min?.text = String(describing: Splitm)
        sec?.text = String(describing: Splits)
        splitmin = Splitm as! Int
        splitsec = Splits as! Int
        
    }
    
    func SplitDidChange(_ Splitm: Split, _ Splits: Split) {
        
        
    }
    func updateChrono(_ timer: Timer) {
        if chronometer.isRunning {
            chrono.text = chronometer.elapsedTimeAsString
        } else {
            timer.invalidate()
        }
    }

    
    
    
    
}


